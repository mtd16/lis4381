> **NOTE:**/ This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Michael DeMaria

### Assignment 1 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation My PHP Installation
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* Git commands w/short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- create a new local repository
2. git status- list files you have changed
3. git add- add one or more files to index
4. git commit- commit changes
5. git push- send changes to the master branch of your remote repos
6. git pull- fetch and merge changes on remote server to working
directory
7. git config- tell Git who you are


#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps_installation.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_installation.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/Android_A1_screenshot.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
