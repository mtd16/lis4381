
# LIS4381

## Michael DeMaria

### Project 2 Requirements:

*Sub-Heading:*

1. Create Server-side validation
2. Add functionality to Edit/Delete buttons from A5

#### README.md file should include the following items:

* Screenshot of Project 2
* Screenshot of serverside validation failed
* Edit page
* RSS feed
* Link to local host website



#### Assignment Screenshots:

*Screenshot of passed validation*:

![Project 2](img/project_2.png)

*Screenshot of edit*
![Error](img/edit.png)

*Screenshot of error*
![Error](img/error.png)

*Screenshot of RSS*
![RSS](img/rss.png)
