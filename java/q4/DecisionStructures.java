import java.util.Scanner;

public class DecisionStructures
{
    public static void main(String[] args)
    {
        System.out.println("Program evaluates user-entered characters.");
        System.out.println("Use following characters: W or w, C or c, H or h, N or n");
        System.out.println("Use following decision structures: if...else, and switch.\n");

        System.out.println("Phone types: W or w (work), C or c (cell), H or h (home), N or n (none).\n");
        System.out.println("Enter phone type:");

        char c = ' ';    //user input;
        Scanner sc = new Scanner(System.in);

        c = sc.next().charAt(0);
        
        //if...else statement output
        System.out.println("\nif...else:");
        System.out.println("Phone type: ");

        if (c == 'W' || c == 'w')
            System.out.println("work");
        else if (c == 'C' || c == 'c')
            System.out.println("cell");
        else if (c == 'H' || c == 'h')
            System.out.println("home");
        else if (c == 'N' || c == 'n')
            System.out.println("none");
        else
            System.out.println("Incorrect character entry.");

        //switch statement output
        System.out.println("\nswitch:");
        System.out.println("Phone type: ");

        switch (c)
        {
            case 'W': case 'w':
                System.out.println("work");
                break;
            case 'C': case 'c':
                System.out.println("cell");
                break;
            case 'H': case 'h':
                System.out.println("home");
                break;
            case 'N': case 'n':
                System.out.println("none");
                break;
            default:
                System.out.println("Incorrect character entry.");
        }
    }
}