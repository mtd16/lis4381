import java.util.Scanner;

public class LargestNumber
{
    public static void main(String args[])
    {
        int num1;
        int num2;
        Scanner in = new Scanner(System.in);
        
        System.out.print("Program evaluates largest of two integers.\nNote: Program does *not* check for non-numberic characters or non-integer values.\n");

        System.out.print("Enter first integer: ");
        num1 = in.nextInt();

        System.out.print("\nEnter second integer: ");
        num2 = in.nextInt();

        if (num1 > num2)
        {
            System.out.print(num1 + " is larger than " + num2);
        }
        else if (num1 < num2)
        {
            System.out.print(num2 + " is larger than " + num1);
        }
        else
        {
            System.out.print("Integers are equal.\n");
        }
    }
}
