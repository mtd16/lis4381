import java.util.Scanner;

public class EvenOrOdd
{
   public static void main(String args[])
   {
       int num;
       Scanner in = new Scanner(System.in);

       System.out.print("Enter an integer: ");
       num = in.nextInt();

       if (num % 2 == 0)
       {
           System.out.print(num + " is an even number.\n");
       }
       else
       {
           System.out.print(num + " is an odd number\n");
       }
   } 
}
