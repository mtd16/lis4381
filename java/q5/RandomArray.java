import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;

class RandomArray {
    public static void main(String args[])
    {
        System.out.println("Program prompts user to enter desired number of psuedorandom-generated integers (min 1).\n");
        System.out.println("Use following loop structures: for, enhanced for, while, do...while.\n\n");

        Scanner sc = new Scanner(System.in);
        Random r = new Random();
        int arraySize = 0;
        int i = 0;

        System.out.println("Enter desired number of psuedorandom-generated integers (min 1): ");
        arraySize = sc.nextInt();
        
        int myArray[] = new int[arraySize];

        //output:for loop
        System.out.println("\nfor loop:");
        for (i = 0; i < myArray.length; i++)
        {
            System.out.println(r.nextInt());
        }

        System.out.println("\nenhanced for loop:");
        for (int n: myArray)
        {
            System.out.println(r.nextInt());
        }

        i = 0;
        System.out.println("\nwhile loop:");
        while (i < myArray.length)
        {
            System.out.println(r.nextInt());
            i++;
        }


        i = 0;
        System.out.println("\ndo...while loop");
        do 
        {
            System.out.println(r.nextInt());
            i++;
        } while (i < myArray.length);


        sc.close();
    }
}