# LIS4381 - Mobile Web Application Development

## Mike DeMaria

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/mtd16/lis4381/src/master/a1/README.md)
    * Install AMPPS
    * Install JDK
    * Install Android Studio and create My First App
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    * Provide git command descriptions
2. [A2 README.md](https://bitbucket.org/mtd16/lis4381/src/master/a2/README.md)
    * Create Healthy Recipes Android app
    * Proide screenshots of completed app
3. [A3 README.md](https://bitbucket.org/mtd16/lis4381/src/master/a3/README.md)
    * Create ERD based upon business rules
    * Provide screenshot of completed ERD
    * Provide DB resource links
4. [P1 README.md](https://bitbucket.org/mtd16/lis4381/src/master/p1/README.md)
    * Backward engineer a business card containg your personal information and interests
    * Provide screenshots of first and second user interface
    * Complete chapter 6 and 7 textbook questions
5. [A4 README.md](https://bitbucket.org/mtd16/lis4381/src/master/a4/README.md)
    * Create an online portfolio of work from assignments and projects completed in this course.
    * Complete chapter 9, 10, and 15 questions
5. [P2 README.md](https://bitbucket.org/mtd16/lis4381/src/master/p2/)
    * Create an online portfolio of work from assignments and projects completed in this course.
    * Complete chapter 9, 10, and 15 questions
