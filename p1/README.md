> **NOTE:**/ This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Michael DeMaria

### Project 1 Requirements:

*Sub-Heading:*

1. Backward engineer a business card mobile application using your photo, contact information, and interests.
2. Create a launcher icon and display it in both screens
3. Add a background color to both screens
4. Add a border around the image and the button
5. Add text shadow to the button

#### README.md file should include the following items:

* Screenshot of first user interface of business card application
* Screenshot of second user interface of business card application
* Textbook questions from chapter 7 and 8


#### Assignment Screenshots and Links:

*Screenshots of Business Card application*

*Screenshot of running application's FIRST user interface http://localhost*:

![First user interface](img/first.png)

*Screenshot of running application's SECOND user interface*

![Second user interface](img/second.png)










