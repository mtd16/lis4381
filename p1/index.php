<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Welcome to my online portfolio, which showcases some of the skills I learned while working on projects in LIS 4381 - Mobile Web Application Development at Florida State University.">
		<meta name="author" content="Michael DeMaria, BSIT Student">
    <link rel="icon" href="favicon.ico">

		<title>LIS 4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Backward engineer a business card mobile application using your photo, contact information, and interests, create a launcher icon and display it in both screens, add a background color to both screens, add a border around the image and the button, add text shadow to the button
				</p>

				<h4>First UI</h4>
				<img src="img/first.png" class="img-responsive center-block" alt="First UI">

				<h4>Second UI</h4>
				<img src="img/second.png" class="img-responsive center-block" alt="Second UI">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
