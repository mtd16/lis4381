
# LIS4381

## Michael DeMaria

### Assignment 4 Requirements:

*Sub-Heading:*

1. Using PHP, JavaScript Bootstrap and jQuery, create an online portfolio showcasing the assignments and projects completed in this class.

#### README.md file should include the following items:

* Screenshot of failed user input validation
* Screenshot of passed user input validation




#### Assignment Screenshots:

*Screenshot of failed validation*

![AMPPS Installation Screenshot](img/failed_validation.png)

*Screenshot of passed validation*:

![JDK Installation Screenshot](img/passed_validation.png)


