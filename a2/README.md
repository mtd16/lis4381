> **NOTE:**/ This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Michael DeMaria

### Assignment 2 Requirements:

*Sub-Heading:*

1. Create a mobile recipe app using Andoid Studio.

#### README.md file should include the following items:

* Screenshots of running Healthy Recipes Android app

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of running application's FIRST user interface http://localhost*:

![First user interface](first.png)

*Screenshot of running application's SECOND user interface*

![Second user interface](second.png)





