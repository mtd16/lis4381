> **NOTE:**/ This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Michael DeMaria

### Assignment 3 Requirements:

*Sub-Heading:*

1. Create a mobile concert ticket value calculator.
2. Build and forward engineer a Pet Store database in MySql.

#### README.md file should include the following items:

* Screenshots of running Concert Ticket App first user interface.
* Screenshots of running Concert Ticket App second user interface.
* Screenshot of Pet Store ERD. 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots and Links:

*Screenshot of Pet Store ERD*

![Pet Store ERD](a3.png)

*Screenshot of running application's FIRST user interface http://localhost*:

![First user interface](first.png)

*Screenshot of running application's SECOND user interface*

![Second user interface](second.png)

*Link to Pet Store MWB file*

[a3.mwb](a3.mwb)

*Link to Pet Store SQL script*

[a3.sql](a3.sql)







